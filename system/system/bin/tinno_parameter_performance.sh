#! /system/bin/sh

config="$1"

#TN Begin modified by keji.sun 202309013 EKFOGO4G-2001 for (heap、lmk、extra_free)
function tinno_heapandlmk_setup()
{
    MemTotalStr=`cat /proc/meminfo | grep MemTotal`
    MemTotal=${MemTotalStr:16:8}

    Heapmaxfree=8m
    Heapminfree=2m	
    Heapstartsize=16m
    Heapsize=512m
    Heapgrowthlimit=384m
	
    Extrafreekbytes=65536
	
    Lmkpsicompletestallms=70
    Lmkswapfreelowpercentage=20
    Lmkthrashinglimit=30
    Lmkthrashinglimitdecay=50	
    Lmkswaputilmax=90
    Lmktimeout=100

    AmsCacheProcessForRamBoost="0G|24,2G|26,4G|28,6G|30,8G|32"
    ZramSizeForRamBoost="0G|55%,2G|60%,4G|70%,6G|80%,8G|90%"
    carrier=$(getprop ro.carrier)
    if [ "$carrier" == "retbr" ]; then
      ZramBoostSizeDefault="8G"
    else
      ZramBoostSizeDefault="4G"
    fi


    if [ $MemTotal -lt 4194430 ]; then
       Heapmaxfree=8m
       Heapminfree=512k
       Heapstartsize=8m
       Heapsize=512m
       Heapgrowthlimit=256m
	   
       Extrafreekbytes=40960
	   
       #lmkd change by 20231110
       Lmkpsicompletestallms=70
       Lmkswapfreelowpercentage=20
       Lmkthrashinglimit=20
       Lmkthrashinglimitdecay=100
       Lmkswaputilmax=90
       Lmktimeout=100
	   
       AmsCacheProcessForRamBoost="0G|12,1G|14,2G|16,4G|18,8G|24"
       ZramSizeForRamBoost="0G|55%,1G|55%,2G|60%,4G|90%,8G|90%"
       ZramBoostSizeDefault="4G"
    fi
	
    #heap parameter set
    setprop persist.sys.tinno.dalvik.vm.heapminfree $Heapminfree
    setprop persist.sys.tinno.dalvik.vm.heapmaxfree $Heapmaxfree
    setprop persist.sys.tinno.dalvik.vm.heapstartsize $Heapstartsize
    setprop persist.sys.tinno.dalvik.vm.heapsize $Heapsize
    setprop persist.sys.tinno.dalvik.vm.heapgrowthlimit $Heapgrowthlimit
    #extra_free_kbytes parameter set	
    setprop persist.sys.tinno.extra_free_kbytes $Extrafreekbytes
    #lmk parameter set	
    setprop persist.sys.tinno.lmk.psi_complete_stall_ms $Lmkpsicompletestallms
    setprop persist.sys.tinno.lmk.swap_free_low_percentage $Lmkswapfreelowpercentage
    setprop persist.sys.tinno.lmk.thrashing_limit $Lmkthrashinglimit
    setprop persist.sys.tinno.lmk.thrashing_limit_decay $Lmkthrashinglimitdecay	
    setprop persist.sys.tinno.lmk.swap_util_max $Lmkswaputilmax
    setprop persist.sys.tinno.lmk.timeout_ms $Lmktimeout	
    setprop sys.tinno.cached_processes $AmsCacheProcessForRamBoost
    setprop sys.tinno.zram_size_overlay $ZramSizeForRamBoost
    setprop persist.sys.default_zram_wb_size $ZramBoostSizeDefault
}

case "$config" in
    "tinno_heapandlmk_setup")
        tinno_heapandlmk_setup
    ;;
       *)

      ;;
esac
