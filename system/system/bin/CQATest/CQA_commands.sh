#!/system/bin/sh

# Copyright (C) 2016 Motorola Mobility, Inc.
# All Rights Reserved

# Please provide implementation for all the functions below.
# Then, push this script to /mnt/sdcard/CQATest/

# Your code must provide return values in text form such as:
# RETURN=PASS
# RETURN=FAIL
# RETURN=<VALUE>

version=0.3

##### READ_TRACK_ID  #####

function READ_TRACK_ID
{
    # insert your code below
    READ_TRACK_ID="FAIL"
    for i in $(seq 5 -1 1)
    do
    if [ $(getprop ro.serial) = "" ];then
    #echo "wait..."
    READ_TRACK_ID="FAIL"
    else
    SN=$(getprop ro.serial)
    READ_TRACK_ID="OK"
	#asciiStr="ABCDEFG"
 
    for c in $(echo $SN|sed 's/./& /g')
    do
        hexArr=$hexArr$(printf "%X" "'$c")
    done

    echo "7E0046180400000000"$hexArr"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000AABB"

    break
    fi
    sleep 0.5
    done
    if [ $READ_TRACK_ID = "FAIL" ];then
    echo "7E0006180400000003AEA3"
    #  echo "feature $0 not implemented"
    fi
}



##### CHECK_POWER_UP  #####

function CHECK_POWER_UP
{
    # insert your code below
    if [ $(getprop sys.boot_completed) = "1" ];then
	echo "7E00081806000000003100AABB"
	else
	echo "7E00081806000000003000AABB"
	fi
}

##### UTAG_SET_BATTERY_ID  #####

function UTAG_SET_BATTERY_ID
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
	
	cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE TEST_FLAG_OP --es CQA_TEST_FUNCTION SETBATTERYSN --es CQA_TEST_PARAMS $1))
	
	sleep 1
    UTAG_SET_BATTERY_ID="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.SETBATTERYSN) -eq 1 ];then
    echo "7E0006250200000000328ACRRC"
    UTAG_SET_BATTERY_ID="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $UTAG_SET_BATTERY_ID = "FAIL" ];then
    echo "7E00062502000000000001CRRC"
    #echo "feature $0 not implemented"
    fi
}

function UTAG_GET_BATTERY_ID
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi

	cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE TEST_FLAG_OP --es CQA_TEST_FUNCTION READBATTERYSN))

	sleep 1
    UTAG_GET_BATTERY_ID="FAIL"
    for i in $(seq 6 -1 1)
    do
	if [ $(getprop persist.sys.READBATTERYSN) = "" ];then
    #echo "wait..."
    UTAG_GET_BATTERY_ID="FAIL"
    else
    SN=$(getprop persist.sys.READBATTERYSN)
	#echo $SN
	#asciiStr="ABCDEFG"

    #for c in $(echo $SN|sed 's/./& /g')
    #do
    #    hexArr=$hexArr$(printf "%X" "'$c")
    #done
    echo "7E0006250300000000"$SN"CRRC"
    UTAG_GET_BATTERY_ID="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $UTAG_GET_BATTERY_ID = "FAIL" ];then
    echo "7E0006250300000001CRRC"
    #echo "feature $0 not implemented"
    fi
}

function UTAG_SET_WALLPAPER
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi

	cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE TEST_FLAG_OP --es CQA_TEST_FUNCTION SETWALLPAPER --es CQA_TEST_PARAMS $1))
	sleep 1
    UTAG_SET_WALLPAPER="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.SETWALLPAPER) -eq 1 ];then
    echo "7E000618020000000000CRRC"
    UTAG_SET_WALLPAPER="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $UTAG_SET_WALLPAPER = "FAIL" ];then
    echo "7E000618020000000001CRRC"
    #echo "feature $0 not implemented"
    fi
}

function UTAG_GET_WALLPAPER
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi

	cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE TEST_FLAG_OP --es CQA_TEST_FUNCTION READWALLPAPER))
	sleep 1
    UTAG_GET_WALLPAPER="FAIL"
    for i in $(seq 6 -1 1)
    do
	if [ $(getprop persist.sys.READWALLPAPER) = "" ];then
    #echo "wait..."
    UTAG_GET_WALLPAPER="FAIL"
    else
    wallpaper=$(getprop persist.sys.READWALLPAPER)
	#echo $wallpaper
	#asciiStr="ABCDEFG"
    for c in $(echo $wallpaper|sed 's/./& /g')
    do
        hexArr=$hexArr$(printf "%X" "'$c")
    done
    echo "7E0006180300000000"$hexArr
    UTAG_GET_WALLPAPER="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $UTAG_GET_WALLPAPER = "FAIL" ];then
    echo "7E0006180300000001CRRC"
    #echo "feature $0 not implemented"
    fi
}



##### GET_TYPE_C_STATE  #####

function GET_TYPE_C_STATE
{
    # insert your code below
	RET=$(cat /sys/devices/platform/extcon_usb/cc_orient)
	if [ $RET = "CC1" ];then
    echo "7E0009090E00000000434331AABB"
	fi

	
	if [ $RET = "CC2" ];then
    echo "7E0009090E00000000434332AABB"
	fi
	
    if [ $RET = "CCNone" ];then
    echo "7E0009090E0000000043434EAABB"
	fi
}

##### READ_BATTERY_THERMISTOR_VALUE  #####

function READ_BATTERY_THERMISTOR_VALUE
{
    # insert your code below
	a=10
    RET=$(cat /sys/class/power_supply/battery/temp)
	#echo $RET
	temp=$(echo $((RET/a)))
	#echo $temp
	hex=$(printf "%x" $temp)
	
	echo "7E0007130500000000"$hex"AABB"
}

function READ_AP_THERMISTOR_VALUE
{
    # insert your code below
	a=1000
    RET=$(cat /sys/class/thermal/thermal_zone11/temp)
	#echo $RET
	temp=$(echo $((RET/a)))
	#echo $temp
	hex=$(printf "%x" $temp)
	
	echo "7E0007130100000000"$hex"AABB"
}

function READ_PCB_THERMISTOR_VALUE
{
    # insert your code below
	echo "not support"
}

function READ_CHARGE_THERMISTOR_VALUE
{
    # insert your code below
	a=1000
    RET=$(cat /sys/class/thermal/thermal_zone30/temp)
	#echo $RET
	temp=$(echo $((RET/a)))
	#echo $temp
	hex=$(printf "%x" $temp)
	
	echo "7E0007130300000000"$hex"AABB"
}


function READ_PA_THERMISTOR_VALUE
{
    # insert your code below
	a=1000
    RET=$(cat /sys/class/thermal/thermal_zone12/temp)
	#echo $RET
	temp=$(echo $((RET/a)))
	#echo $temp
	hex=$(printf "%x" $temp)
	
	echo "7E0007130400000000"$hex"AABB"
}

function READ_CPU_THERMISTOR_VALUE
{
    # insert your code below
	a=1000
    RET=$(cat /sys/class/thermal/thermal_zone5/temp)
	#echo $RET
	temp=$(echo $((RET/a)))
	#echo $temp
	hex=$(printf "%x" $temp)
	
	echo "7E0007130600000000"$hex"AABB"
}

##### START_TOUCHSCREEN_TEST  #####
function START_TOUCHSCREEN_TEST
{
    # insert your code below
    #RESULT_FILE="/sdcard/testresult.txt"
    PASS_WORD="MP TEST PASS"
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
    fi
    sleep 0.5
    RET=$(cat /proc/touch_info/tp_selftest_result)
    #echo $RET
    sleep 3

    #RET=$(grep "$PASS_WORD" $RESULT_FILE)
    #echo $RET

    result=$(echo $RET | grep "$PASS_WORD")
    if [[ "$result" != "" ]]; then
    #if [ $RET  "$PASS_WORD" ];then
        echo "7E0006190100000000AABB"
    else
        echo "7E0006190100000001AABB"
    fi
}

##### ACCLEROMETER_EXECUTE_OFFSET_CALIBRATION  #####
function ACCLEROMETER_EXECUTE_OFFSET_CALIBRATION
{
    # insert your code below
	RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE SENSOR --es CQA_TEST_FUNCTION SENSOR_CAL_ACCEL >> /dev/null 2>&1
    sleep 3
    SENSOR_CAL_ACCEL="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.SENSOR_CAL_ACCEL) -eq 1 ];then
    echo "7E0006010200000000CRRC"
    SENSOR_CAL_ACCEL="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_CAL_ACCEL = "FAIL" ];then
    echo "7E0006010200000001CRRC"
    #echo "feature $0 not implemented"
    fi
}

##### ACCLEROMETER_READ_OFFSET  #####
function ACCLEROMETER_READ_OFFSET
{
   # insert your code below
   RET=$(cat /mnt/vendor/nvcfg/sensor/acc_cali.json)
   #echo $RET
   result=$(echo $RET | grep -oE '[0-9-]*')
   #echo  $result
   x=$(echo  $result | awk '{ print $1 }')
   #echo  $x
   y=$(echo  $result | awk '{ print $2 }')
   #echo  $y
   z=$(echo  $result | awk '{ print $3 }')
   #echo  $z
   
   hexX=$(printf "%8x" $x)
   hexXX=$(echo $(printf "%08s\n" $hexX))
   #echo ${hexXX:0-0:8}
   hexY=$(printf "%x" $y)
   hexYY=$(echo $(printf "%08s\n" $hexY))
   #echo ${hexYY:0-0:8}
   #echo $(printf "%08s\n" $hexY)
   hexZ=$(printf "%x" $z)
   #echo $(printf "%08s\n" $hexZ)
   hexZZ=$(echo $(printf "%08s\n" $hexZ))
   #echo ${hexZ: -8}
   echo "7E0012010300000000""${hexXX: -8}""${hexYY: -8}""${hexZZ: -8}""CRRC"
}


function GYROSCOPE_EXECUTE_OFFSET_CALIBRATION
{
    # insert your code below
	
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
	
    am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE SENSOR --es CQA_TEST_FUNCTION SENSOR_CAL_GYRO >> /dev/null 2>&1
    sleep 3
    SENSOR_CAL_GRY="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.SENSOR_CAL_GRY) -eq 1 ];then
    echo "7E0006010500000000CRRC"
    SENSOR_CAL_ACCEL="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_CAL_GRY = "FAIL" ];then
    echo "7E0006010500000001CRRC"
    fi
}

function GYROSCOPE_READ_OFFSET
{
   # insert your code below
   RET=$(cat /mnt/vendor/nvcfg/sensor/gyro_cali.json)
   #echo $RET
   result=$(echo $RET | grep -oE '[0-9-]*')
   #echo  $result
   x=$(echo  $result | awk '{ print $1 }')
   #echo  $x
   y=$(echo  $result | awk '{ print $2 }')
  # echo  $y
   z=$(echo  $result | awk '{ print $3 }')
   #echo  $z
   
   hexX=$(printf "%8x" $x)
   hexXX=$(echo $(printf "%08s\n" $hexX))
   #echo ${hexXX:0-0:8}
   hexY=$(printf "%x" $y)
   hexYY=$(echo $(printf "%08s\n" $hexY))
   #echo ${hexYY:0-0:8}
   #echo $(printf "%08s\n" $hexY)
   hexZ=$(printf "%x" $z)
   #echo $(printf "%08s\n" $hexZ)
   hexZZ=$(echo $(printf "%08s\n" $hexZ))
   #echo ${hexZ: -8}
   echo "7E0012010600000000""${hexXX: -8}""${hexYY: -8}""${hexZZ: -8}""CRRC"
}


function ENABLE_PROXIMITY_SENSOR
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.ProxSensorFtm >> /dev/null 2>&1
	echo "7E0006260100000000CRRC"
}


function PROX_CROSSTALK_CALIBRATION
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.ProxSensorFtm --es action docaliuncover >> /dev/null 2>&1
    sleep 3
    SENSOR_CAL_PROXIMITY="FAIL"
    for i in $(seq 9 -1 1)
    do
    if [ $(getprop persist.sys.SENSOR_CAL_PROXIMITY) -eq 1 ];then
    echo "7E0006260300000000CRRC"
    SENSOR_CAL_PROXIMITY="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_CAL_PROXIMITY = "FAIL" ];then
    echo "7E0006260300000001CRRC"
    #echo "feature $0 not implemented"
    fi
}

function PROX_READ_COVER_RAWDATA
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.ProxSensorFtm --es action getrawdata >> /dev/null 2>&1
    sleep 1.5
    SENSOR_PROXIMITY_RAWDATA="FAIL"
    for i in $(seq 9 -1 1)
    do
    if [ $(getprop persist.sys.SENSOR_PROXIMITY_RAWDATA) -ne 0 ];then
	rawdata=$(getprop persist.sys.SENSOR_PROXIMITY_RAWDATA)
	hexX=$(printf "%8x" $rawdata)
	#echo $hexX
    hexXX=$(echo $(printf "%08s\n" $hexX))
	#echo $hexXX
    #echo ${hexXX:0-0:8}
    echo "7E000A260400000000""${hexXX: -8}""CRRC"
    SENSOR_PROXIMITY_RAWDATA="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_PROXIMITY_RAWDATA = "FAIL" ];then
    echo "7E000A260400000001CRRC"
    #echo "feature $0 not implemented"
    fi
}



function WRITE_PROX_UNCOVER_RAWDATA
{
    # insert your code below
	echo "7E0006260500000000CRRC"
}

function PROX_WRITE_COVER_RAWDATA
{
    # insert your code below
	echo "7E0006260500000000CRRC"
}

function PROX_EXECUTE_THRESHOLD_CALIBRATION
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.ProxSensorFtm --es action docalicover >> /dev/null 2>&1
    sleep 2
    SENSOR_CAL_PROXIMITY_2CM="FAIL"
    for i in $(seq 9 -1 1)
    do
    if [ $(getprop persist.sys.SENSOR_CAL_PROXIMITY_2CM) -eq 1 ];then
    echo "7E0006260600000000CRRC" #PASS
    SENSOR_CAL_PROXIMITY_2CM="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_CAL_PROXIMITY_2CM = "FAIL" ];then
    echo "7E0006260600000001CRRC" #FAIL
    #echo "feature $0 not implemented"
    fi
}

function PROX_READ_THRESHOLD
{
   # insert your code below
   RET=$(cat /mnt/vendor/nvcfg/sensor/ps_cali.json)
   #echo $RET
   result=$(echo $RET | grep -oE '[0-9-]*')
   #echo  $result
   x=$(echo  $result | awk '{ print $1 }')
   #echo  $x
   y=$(echo  $result | awk '{ print $2 }')

   
   hexX=$(printf "%8x" $x)
   hexXX=$(echo $(printf "%08s\n" $hexX))
   #echo ${hexXX:0-0:8}
   hexY=$(printf "%x" $y)
   hexYY=$(echo $(printf "%08s\n" $hexY))
   #echo ${hexYY:0-0:8}
   #echo $(printf "%08s\n" $hexY)
   echo "7E000E260700000000""${hexXX: -8}""${hexYY: -8}""CRRC"
}

function DISABLE_PROXIMITY_SENSOR
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.ProxSensorFtm --es action finish >> /dev/null 2>&1
    echo "7E0006260200000000CRRC"
}


function ENABLE_ALS_SENSOR
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.LightSensorFtm >> /dev/null 2>&1
    echo "7E0006020500000000CRRC"
}

function LIGHT_SENSOR_READ_FROM_PHONE_3CH
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.LightSensorFtm --es action getdata  >> /dev/null 2>&1
	sleep 1
    SENSOR_LSENSOR_RAWDATA="FAIL"
    for i in $(seq 9 -1 1)
    do
	rawdata=$(getprop persist.sys.SENSOR_LSENSOR_RAWDATA)
	#echo $rawdata
    if [ $rawdata -ne 0 ];then
	result=$(echo $rawdata | grep -oE '[0-9-]*')
	
   x=$(echo  $result | awk '{ print $1 }')
   #echo  $x
   y=$(echo  $result | awk '{ print $2 }')
   #echo  $y
   z=$(echo  $result | awk '{ print $3 }')
   #echo  $z
   
   hexX=$(printf "%8x" $x)
   hexXX=$(echo $(printf "%08s\n" $hexX))
   #echo ${hexXX:0-0:8}
   hexY=$(printf "%x" $y)
   hexYY=$(echo $(printf "%08s\n" $hexY))
   #echo ${hexYY:0-0:8}
   #echo $(printf "%08s\n" $hexY)
   hexZ=$(printf "%x" $z)
   #echo $(printf "%08s\n" $hexZ)
   hexZZ=$(echo $(printf "%08s\n" $hexZ))
   #echo ${hexZ: -8}
   echo "7E0012020100000000""${hexXX: -8}""${hexYY: -8}""${hexZZ: -8}""CRRC"

    SENSOR_LSENSOR_RAWDATA="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_LSENSOR_RAWDATA = "FAIL" ];then
    echo "7E0006260600000001CRRC" #FAIL
    #echo "feature $0 not implemented"
    fi
}

function LIGHT_SENSOR_CALIBRATION_WRITE_TARGET
{
    # insert your code below
    echo "7E0006020200000000CRRC"
}

function LIGHT_SENSOR_CALIBRATION_EXCUTE_CALI
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
	am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.LightSensorFtm --es action doCali >> /dev/null 2>&1
	SENSOR_LSENSOR_CAL="FAIL"
    for i in $(seq 9 -1 1)
    do
    if [ $(getprop persist.sys.SENSOR_LSENSOR_CAL) -eq 1 ];then
    echo "7E0006020300000000CRRC" #PASS
    SENSOR_LSENSOR_CAL="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $SENSOR_LSENSOR_CAL = "FAIL" ];then
    echo "7E0006020300000001CRRC" #FAIL
    #echo "feature $0 not implemented"
    fi
}


function LIGHT_SENSOR_CALIBRATION_VERIFY_COEFFICIENTS
{
   # insert your code below
   RET=$(cat /mnt/vendor/nvcfg/sensor/als_cali.json)
   #echo $RET
   result=$(echo $RET | grep -oE '[0-9-]*')
   #echo  $result
   x=$(echo  $result | awk '{ print $1 }')
   
   hexX=$(printf "%8x" $x)
   hexXX=$(echo $(printf "%08s\n" $hexX))
   #echo ${hexXX:0-0:8}
   echo "7E000E260700000000""${hexXX: -8}""CRRC"
}

function DISABLE_ALS_SENSOR
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
    am start -n com.mediatek.sensorhub.ui/com.mediatek.sensorhub.sensor.alspsex.LightSensorFtm --es action finish >> /dev/null 2>&1
    echo "7E0006020600000000CRRC"
}

function FPS_ON_SENSOR
{
    # insert your code below
    setprop persist.sys.power_disable true
    am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE  FINGERPRINT --es CQA_TEST_FUNCTION FINGERPRINT_PRESENCE >> /dev/null 2>&1
    sleep 2
    FINGERPRINT_PRESENCE="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.FINGERPRINT_PRESENCE) -eq 1 ];then
    echo "7E0006020600000011CRRC"
    FINGERPRINT_PRESENCE="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $FINGERPRINT_PRESENCE = "FAIL" ];then
    echo "7E0006020600000010CRRC"
    # echo "feature $0 not implemented"
    fi
    setprop persist.sys.power_disable false
}



function DISABLE_NFC
{
    echo "feature $0 not implemented"
    echo "1"
}

function RESET_NFC
{
    # insert your code below
    echo "feature $0 not implemented"
    echo "1"
}

function ENABLE_NFC_TEST_MODE
{
    # insert your code below
    cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE NFC --es CQA_TEST_FUNCTION NFCTN))
	NFCTN="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.NFCTN) -eq 1 ];then
    echo "7E0006120300000001AABB"
    NFCTN="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $NFCTN = "FAIL" ];then
    echo "7E0006120300000000AABB"
    # echo "feature $0 not implemented"
    fi
}

function START_NFC_SWP_SELF_TEST_BOARD
{
    # insert your code below
    echo "feature $0 not implemented"
    echo "1"
}


function NFC_ANTENNA_SELFTEST
{
    # insert your code below
    echo "feature $0 not implemented"
    echo "1"
}

function READ_LCD_VENDOR_INFO
{
    # insert your code below
    RET=$(cat /sys/devices/platform/product-device-info/info_lcd)
    if [ $RET == "BOE-ILI9883C-VDO" ]
	then echo "7E00070D010000000001CRRC"
	fi
	if [ $RET == "DJIN-FT8057S-VDO" ]
	then echo "7E00070D010000000002CRRC"
	fi
}

function READ_NVM_SIZE
{
    # insert your code below
    RET=$(cat /sys/block/mmcblk0/size)
	hex=$(printf "%x" $RET)
	echo "7E000A0A0200000000"$hex"CRRC"
	
}


function READ_SDCARD_SIZE
{
   # insert your code below
   strA="media_rw"
   
   RET=$(df | grep "media_rw")
   #echo $RET
   
   if [ "$RET" =  "" ];then
   echo "7E00062102000000000AABB"
   else
   size=$(echo $RET | awk '{ print $2 }')
   hex=$(printf "%08x" $size)
   echo "7E000621020"$hex"ABBA"
   fi
}



function GET_RAM_LPDDR_SIZE
{
    # insert your code below
	a=1024
    RET=$(cat /proc/meminfo | grep MemTotal)
	mem=$(echo $RET | grep -oE '[0-9]*')
	echo $((mem/a))
}


function READ_SW_VERSION
{
    # insert your code below
    echo $(getprop ro.build.description)
    #  echo "feature $0 not implemented"
}

function GET_MMC_ABSENT_STATUS
{
    # insert your code below
    #RET=$(cat /sys/devices/platform/11240000.mmc/mmc_host/mmc1/sd_state)
    if [ $(cat /sys/devices/platform/11240000.mmc/mmc_host/mmc1/sd_state) = "1" ];then
	echo "7E000721010000000001AABB"
	else
	echo "7E000721010000000000AABB"
	fi
}

function SWITCH_POWER_SOURCE_TO_BATTERY
{
    # insert your code below
    echo 0 1 >/proc/mtk_battery_cmd/current_cmd
    echo "7E0006090100000000AABB"
}

function SWITCH_POWER_SOURCE_TO_CHARGE
{
    # insert your code below
    echo 0 0 >/proc/mtk_battery_cmd/current_cmd
    echo "7E0007090200000000AABB"
}

function QUICK_STANDBY
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -gt 0 ];then
    input keyevent 26
	fi

	echo "7E0006180300000000AABB"
}

function READ_VBUS_VOLTAGE
{
    # insert your code below
    RET=$(cat /sys/devices/platform/charger/ADC_Charger_Voltage)
    hex=$(printf "%x" $RET)

	echo "7E000618030000"$hex"AABB"
}


function DISABLE_BATTERY_FET
{
    # insert your code below
	echo "not support"
}

function ENABLE_PATH_FET_TO_BATTERY
{
    # insert your code below
	echo "not support"
}

##### READ_VBAT_VOLTAGE  #####

function READ_VBAT_VOLTAGE
{
    # insert your code below
    RET=$(cat /sys/class/power_supply/battery/voltage_now)
    hex=$(printf "%x" $RET)

	echo "7E000A09070000000000"$hex"AABB"
}

function SET_INPUT_PATH_TO_3_AMPS
{
    # insert your code below
    echo 3000 > /sys/class/power_supply/mtk-master-charger/input_current_limit

	echo "7E0007090A00000000AABB"
}

function SET_OUTPUT_PATH_TO_1_AMPS
{
    # insert your code below
    echo 1000 > /sys/class/power_supply/mtk-master-charger/constant_charge_current_max

	echo "7E0007090B00000000AABB"
}


function GET_SIM_ABSENT_STATUS
{
    # insert your code below
    RET=$(cat /sys/devices/platform/regulator_vibrator/slot_state)
    if [ $RET -eq 1 ];then
	echo "7E000727030000000001CRRC"
	else
	echo "7E000727030000000000CRRC"
	fi
}

function ENABLE_WLAN
{
    # insert your code below
    svc wifi enable
	echo "7E0006240100000000AABB"
}

function DISABLE_WLAN
{
    # insert your code below
    svc wifi disable
	echo "7E0006240200000000AABB"
}

function ENABLE_BT
{
    # insert your code below
    svc bluetooth enable
	echo "7E0002070100000000AABB"
}

function DISABLE_BT
{
    # insert your code below
    svc bluetooth disable
	echo "7E0002070200000000AABB"
}

function ENABLE_NFC
{
    # insert your code below
    svc nfc enable
	echo "7E0006120100000000AABB"
}

function DISABLE_NFC
{
    # insert your code below
    svc nfc disable
	echo "7E0006120200000000AABB"
}


function CAP_SENSOR_DETECT
{
    # insert your code below
	if [ -f /sys/bus/i2c/drivers/awinic_sar/3-0012/diff ];then
	size=$(cat /sys/bus/i2c/drivers/awinic_sar/3-0012/diff | awk '{ print $3 }')
	#echo $size

	result=(${size// /})
	detect="FAIL"
	
	for v in ${result[@]};do
		if [ $v -ne 0 ];then
		detect="PASS"
		break
		fi
    done
	
	if [ $detect = "PASS" ];then
	echo "7E0006080100000000CRRC"
	else
	echo "7E0006080100000011CRRC"
	fi
	
	else
	echo "7E0006080100000011CRRC"
    fi
}

function CAP_SENSOR_ENABLE
{
    # insert your code below
	if [ -f /sys/bus/i2c/drivers/awinic_sar/3-0012/mode_operation ];then
    echo 1 > /sys/bus/i2c/drivers/awinic_sar/3-0012/mode_operation
	echo "7E0006080200000000CRRC"
    else
    echo "7E0006080200000011CRRC"
    fi
}

function CAP_SENSOR_EXECUTE_SELF_CALIBRATION
{
    # insert your code below
    if [ -f /sys/bus/i2c/drivers/awinic_sar/3-0012/aot ];then
    echo 1 > /sys/bus/i2c/drivers/awinic_sar/3-0012/aot
	echo "7E0006080400000000CRRC"
    else
    echo "7E0006080400000011CRRC"
    fi
}

function CAP_SENSOR_READ_DIFF_VALUE
{
    # insert your code below
	if [ -f /sys/bus/i2c/drivers/awinic_sar/3-0012/diff ];then
	size=$(cat /sys/bus/i2c/drivers/awinic_sar/3-0012/diff | awk '{ print $3 }')
	#echo $size

	result=(${size// /})
	diffret=""
	
	for v in ${result[@]};do
        hexX=$(printf "%8x" $v)
        hexXX=$(echo $(printf "%08s\n" $hexX))
		v=$(echo ${hexXX: -8})
		diffret=$diffret$v
		#echo $v
    done
	#echo "${result[@]}"
	#echo $diffret
	echo "7E001E080700000000"$diffret"CRRC"
	else
	echo "7E0006080700000011CRRC"
    fi
}


function CAP_SENSOR_CHECK_CALI_VALUE_PHONE
{
    # insert your code below
	if [ -f /sys/bus/i2c/drivers/awinic_sar/3-0012/offset ];then
	size=$(cat /sys/bus/i2c/drivers/awinic_sar/3-0012/offset | awk '{ print $3 }')
	#echo $size

	result=(${size// /})
	diffret=""
	a=1000
	for v in ${result[@]};do
	    result="$v"| awk '{printf "%.3f", $0}'
		v=$(echo $v $a | awk '{printf("%.0f",$1*$2)}')
		
		#echo $v
        hexX=$(printf "%8x" $v)
        hexXX=$(echo $(printf "%08s\n" $hexX))
		v=$(echo ${hexXX: -8})
		diffret=$diffret$v
		#echo $v
    done
	#echo "${result[@]}"
	#echo $diffret
	echo "7E0006080500000000"$diffret"CRRC"
	else
	echo "7E0006080500000011CRRC"
    fi
}

function CAP_SENSOR_DISABLE
{
    # insert your code below
    if [ -f /sys/bus/i2c/drivers/awinic_sar/3-0012/mode_operation ];then
    echo 2 > /sys/bus/i2c/drivers/awinic_sar/3-0012/mode_operation
	echo "7E0006080300000000CRRC"
    else
    echo "7E0006080300000011CRRC"
    fi
}

function RESET_TEST_FLAG
{
    # insert your code below
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
	
    cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE  TEST_FLAG_OP --es CQA_TEST_FUNCTION RESETTAG))
    sleep 1
    RESETTAG="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.RESETTAG) -eq 1 ];then
    echo "7E0046180100000001AABB"
    RESETTAG="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $RESETTAG = "FAIL" ];then
    echo "7E0006020600000010CRRC"
    # echo "feature $0 not implemented"
    fi
}

function WRITE_TEST_FLAG
{
    # insert your code below
	
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
	
	tag=$1
	#echo $tag
	bt="01000000000000000000000000000000"
	L2="01000000000000001000000000000000"
	ar="01000000000000001100000000000000"
	
    if [ $tag = $bt ];then
		cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE  TEST_FLAG_OP --es CQA_TEST_FUNCTION WRITETAGBT))
		sleep 1
		WRITETAGBT="FAIL"
		for i in $(seq 6 -1 1)
		do
		if [ $(getprop persist.sys.WRITETAGBT) -eq 1 ];then
		echo "7E0046180300000000AABB"
		WRITETAGBT="OK"
		break
		#else
		#echo "wait..."
		fi
		sleep 1
		done
		if [ $WRITETAGBT = "FAIL" ];then
		echo "7E0046180300000001AABB"
		# echo "feature $0 not implemented"
		fi
	fi
	
	if [ $tag = $L2 ];then
		cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE  TEST_FLAG_OP --es CQA_TEST_FUNCTION WRITETAGL2vision))
		sleep 1
		WRITETAGL2vision="FAIL"
		for i in $(seq 6 -1 1)
		do
		if [ $(getprop persist.sys.WRITETAGL2vision) -eq 1 ];then
		echo "7E0046180300000000AABB"
		WRITETAGL2vision="OK"
		break
		#else
		#echo "wait..."
		fi
		sleep 1
		done
		if [ $WRITETAGL2vision = "FAIL" ];then
		echo "7E0046180300000001AABB"
		# echo "feature $0 not implemented"
		fi
	fi
	
    if [ $tag = $ar ];then
	#echo testar
		cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE  TEST_FLAG_OP --es CQA_TEST_FUNCTION WRITETAGAR))
		sleep 1
		WRITETAGAR="FAIL"
		for i in $(seq 6 -1 1)
		do
		if [ $(getprop persist.sys.WRITETAGAR) -eq 1 ];then
		echo "7E0046180300000000AABB"
		WRITETAGAR="OK"
		break
		#else
		#echo "wait..."
		fi
		sleep 1
		done
		if [ $WRITETAGAR = "FAIL" ];then
		echo "7E0046180300000001AABB"
		# echo "feature $0 not implemented"
		fi
	fi

}

function READ_TEST_FLAG
{
    # insert your code below
	
    RET=$(cat /sys/class/leds/lcd-backlight/brightness)
    if [ $RET -eq 0 ];then
    input keyevent 26
	sleep 0.5
    fi
	
	cqatest=$(echo $(am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE  TEST_FLAG_OP --es CQA_TEST_FUNCTION READTAG))
	
	sleep 1
	
	bt=$(getprop persist.sys.READTAGBT)
	l2=$(getprop persist.sys.READTAGL2vision)
	ar=$(getprop persist.sys.READTAGAR)
	
	readtag=$(getprop persist.sys.READTAG)
	
	if [ -z "$readtag" ];then
	
		#echo $bt$l2$ar
		
		if [ $bt -eq 1 ];then
		bt="100000000000000"
		else
		bt="000000000000000"
		fi
		
		if [ $l2 -eq 1 ];then
		l2="1"
		else
		l2="0"
		fi
		
		if [ $ar -eq 1 ];then
		ar="100000000000000"
		else
		ar="000000000000000"
		fi
		
		echo "7E00461802000000000"$bt$l2$ar"AABB"
	else
		echo "7E004618020000000100000000000000000000000000000000AABB"
	
	fi
}


###################################################################
# Function:    FM_ON                                              #
# Description: This function should enable the FM module          #
#              and sync default frequency of 97.5Mhz              #
# Note:        no splash screen or prompt box should be required  #
# Inputs:      N/A                                                #
# Output:      status:     OK/FAIL                               #
###################################################################
function FM_ON
{
    # insert your code below
    am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE FM --es CQA_TEST_FUNCTION FM_ON
    sleep 1
    FM_ON="FAIL"
    for i in $(seq 15 -1 1)
    do
    if [ $(getprop persist.sys.FM_STATE_ON) -eq 1 ];then
    FM_ON="OK"
    echo "RETURN=PASS"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $FM_ON = "FAIL" ];then
    echo "RETURN=FAIL"
    echo "feature $0 not implemented"
    fi
  
}

###################################################################
# Function:    FM_TUNE                                            #
# Description: This function should sync a desired freq passed as #
#              input on this function call                        #
# Inputs:      Desired FM freq                                    #
# Output:      status:     OK/FAIL                               #
###################################################################
function FM_TUNE
{
    # takes argument
    # insert your code below
    am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE FM --es CQA_TEST_FUNCTION FM_TUNE --es CQA_TEST_PARAMS $1
    
    #am broadcast -a com.ape.factory.FM_GETRSSI --es FMtune $1
    sleep 1
    FM_TUNE="FAIL"
    for i in $(seq 3 -1 1)
    do
    if [ $(getprop persist.sys.FM_TuneState) -eq 1 ];then
    echo "parameter received: $1"
    echo "RETURN=PASS"
    FM_TUNE="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $FM_TUNE = "FAIL" ];then
    echo "parameter received: $1"
    echo "RETURN=FAIL"
    #echo "feature $0 not implemented"
    fi
}

###################################################################
# Function:    FM_GETRSSI                                         #
# Description: This function should return the FM RSSI            #
# Note:        The RSSI should be updated periodically or on      #
#              every call of function FM_GETRSSI                  #
# Inputs:      N/A                                                #
# Output:      OK,[FM rssi]/FAIL                                 #
###################################################################
function FM_GETRSSI
{
    # insert your code below
    am broadcast -a com.ape.factory.FM_GETRSSI
    sleep 1
    FM_GETRSSI="FAIL"
    for i in $(seq 15 -1 1)
    do
    if [ $(getprop persist.sys.RSSI_VALUE) -ne 0 ];then
    echo "RETURN=PASS,[$(getprop persist.sys.RSSI_VALUE)]"
    FM_GETRSSI="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $FM_GETRSSI = "FAIL" ];then
    echo "RETURN=FAIL"
    #echo "feature $0 not implemented"
    fi
}

###################################################################
# Function:    FM_OFF                                             #
# Description: This function should disable the FM module         #
# Note:        terminate any FM process                           #
# Inputs:      N/A                                                #
# Output:      status:     OK/FAIL                               #
###################################################################
function FM_OFF
{
    # insert your code below
    am start -n com.ape.factory/.CQAtest.CQAActivity -S --es CQA_TEST_MODE FM --es CQA_TEST_FUNCTION FM_OFF
    sleep 1
    FM_OFF="FAIL"
    for i in $(seq 15 -1 1)
    do
    if [ $(getprop persist.sys.FM_STATE_OFF) -eq 1 ];then
    FM_OFF="OK"
    echo "RETURN=PASS"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $FM_OFF = "FAIL" ];then
    echo "RETURN=FAIL"
    echo "feature $0 not implemented"
    fi
}

###################################################################
# Function:    FM_CHECK                                           #
# Description: This function should check FM (SOC or not.)        #
# Inputs:      N/A                                                #
# Output:      status:     OK/FAIL                               #
###################################################################
function FM_CHECK
{
    sleep 1
    FM_CHECK="FAIL"
    for i in $(seq 6 -1 1)
    do
    if [ $(getprop persist.sys.FTMFM_state) -eq 1 ];then
    echo "RETURN=PASS"
    FM_CHECK="OK"
    break
    #else
    #echo "wait..."
    fi
    sleep 1
    done
    if [ $FM_CHECK = "FAIL" ];then
    echo "RETURN=FAIL"
    fi
}

##### Other - END #####

##### Main #####
#echo
#echo "CQA Commands - Version $version"
#echo
#echo "Command executed - \"$0 $@\""
#echo
eval $@
