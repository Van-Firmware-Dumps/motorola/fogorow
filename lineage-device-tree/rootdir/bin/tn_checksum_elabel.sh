#!/vendor/bin/sh

#
# Copyright (c) TN Technologies Co., Ltd. 2023-2023. All rights reserved.
#
# CHECK SUM MD5 script.
# linhong.han
# 2023-12-25
#

ELABEL_FILE="mnt/elabel/regulatory_info_%s.png"
md5=$(md5sum $ELABEL_FILE | cut -d' ' -f1)
echo $md5

#setprop persist.vendor.elabel.elabel_md5 $md5
setprop ro.vendor.elabel.md5 $md5
setprop persist.vendor.elabel.stop_check 1

