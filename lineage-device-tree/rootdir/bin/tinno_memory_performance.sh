#!/vendor/bin/sh

config="$1"

#TN Begin modified by keji.sun 202309013 EKFOGO4G-2001 for (swappiness、watermark)
function read_ahead_kb_values_setup() {
	MemTotalStr=`cat /proc/meminfo | grep MemTotal`
	MemTotal=${MemTotalStr:16:8}
	erofs_ra_kb=128
	
	dmpts=$(ls /sys/block/*/queue/read_ahead_kb | grep -e dm -e mmc)

	# Set 128 for <= 3GB &
	# set 512 for >= 4GB targets.
	if [ $MemTotal -le 4194430 ]; then
		ra_kb=128
	else
		ra_kb=512
	fi

	if [ -f /sys/block/mmcblk0/bdi/read_ahead_kb ]; then
		echo $ra_kb > /sys/block/mmcblk0/bdi/read_ahead_kb
	fi
	if [ -f /sys/block/mmcblk0rpmb/bdi/read_ahead_kb ]; then
		echo $ra_kb > /sys/block/mmcblk0rpmb/bdi/read_ahead_kb
	fi

	for dm in $dmpts; do
		dm_dev=`echo $dm |cut -d/ -f4`
		if [ "$dm_dev" = "" ]; then
			is_erofs=""
		else
			is_erofs=`mount |grep erofs |grep "${dm_dev} "`
		fi

		if [ "$is_erofs" = "" ]; then
			echo $ra_kb > $dm
		else
			echo $erofs_ra_kb > $dm
		fi
	done
}

function swappiness_watermark_parameters_setup() {
	MemTotalStr=`cat /proc/meminfo | grep MemTotal`
	MemTotal=${MemTotalStr:16:8}
	
	Wmarkminfreekbytes=11138
	Watermarkscalefactor=100
	
	Wmarkswappiness=60

	Dirtyratio=20
	Dirtybackgroundratio=10

    if [ $MemTotal -lt 4194430 ]; then
	   Wmarkminfreekbytes=7685
	   Watermarkscalefactor=100
	   Wmarkswappiness=160
	   Dirtyratio=20
	   Dirtybackgroundratio=5
    fi

	#set watermark
	echo $Watermarkscalefactor > /proc/sys/vm/watermark_scale_factor
	echo $Wmarkminfreekbytes > /proc/sys/vm/min_free_kbytes
	#set swappiness
	echo $Wmarkswappiness > /proc/sys/vm/swappiness
	echo $Wmarkswappiness > /dev/memcg/apps/memory.swappiness
	echo $Wmarkswappiness > /dev/memcg/system/memory.swappiness
	#set dirty
	echo $Dirtybackgroundratio > /proc/sys/vm/dirty_background_ratio
	echo $Dirtyratio > /proc/sys/vm/dirty_ratio 
	
	
}

# TN Begin modified by keji.sun 20231110 EKFOGO4G-2387  for (kswapd cpu taskset )
function taskset_kswapd_cpu() {
    #kswapd0 cpu taskset small core
	taskset -ap 3f `pidof -x kswapd0`
	taskset -ap 3f `pidof -x kcompactd0`
}

case "$config" in
	"swappiness_watermark_parameters_setup")
		echo "swappiness_watermark_parameters_setup"
		swappiness_watermark_parameters_setup
		;;
	"read_ahead_kb_values_setup")
		echo "read_ahead_kb_values_setup"
		read_ahead_kb_values_setup
		;;
	"taskset_kswapd_cpu")
		echo "taskset_kswapd_cpu"
		taskset_kswapd_cpu
		;;		
esac
